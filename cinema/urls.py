"""
Definition of urls for cinema.
"""
from django.contrib import admin
from django.urls import path
from datetime import datetime
from django.conf.urls import url, include
import django.contrib.auth.views



# Uncomment the next lines to enable the admin:
# from django.conf.urls import include
from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    # Examples:
    path('admin/', admin.site.urls),
    url(r'^films/',include('film.urls')),
    
    
]
