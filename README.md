# Projet devops Cinema

devops tu coco
      
  Bienvenue dans notre projet de Devops d'ING 4 à ECE Paris !!

Bonus : utilisation de Python, base de donnée mySql...

   I / l'API

API créé en python (utilisation de Django)

  Nous avons décidés de réaliser une application de gestion de Film.
Nous offrons dans cette application web la possibilité aux utilisateur de se connecter à leurs compte et suivre les nouveaux films du site.

Une Fois toute la configuration réalisée, lorsqu'on se connecte en localhost sur le port 4000, localhost:4000 
nous arrivons sur la page des films de l'application web et localhost:4000\admin à la page d'administration.

Il y'a deux partie dans cette application, Film et Admin.
Film permet d'avoi accès à une liste de film, et Admin permet la création de compte et l'ajout de film. Nous pouvons nous connecter avec notre compte pour l'ajout et suppression de film.
Les données sont stockés dans une base de données MySql, et l'API possède les fonctionnalité CRUD. 
Les fonctionnalité CRUD sont l'update, create, delate et read.

Réalisation des tests :

Nous avons créé un fichier de test possédant plusieurs test pour la connexion, et l'utilisation des diférentes fonctionnalitées.

Pré-requis:

Python3.7
Mysql

Installation:

1/ Créer un environnement virtuel

> pip install <directory> -p <path python.exe>
	
2/ Activer l'environnement virtuel

><directory de l'environnement>\Scripts\activate.bat

3/ Installer les librairies python nécessaires à l'application

> pip install django==3.1
> pip install mysqlclient

Configuration de l'application :

1/ Créer une base de données (ex: cinema)

2/ Editer le fichier de configuration de l'application web qui se trouve dans le répertoire du projet

	-> Paramétrer la configuration de la base de données:
	DATABASES = {
   	 'default': {
         'ENGINE': 'django.db.backends.mysql',
         'NAME': 'cinema',
		'USER': '<user>',
		'PASSWORD': '<password>',
		'HOST': '127.0.0.1',
		'PORT': '3306',
    	}
	}

3/ Migrer les tables par défaut du framework Django et les tables de l'application web
Ouvrir une invite de commande et se déplacer dans le répertoire de l'application où se trouve le fichier manage.py
appeller la commande suivante:

> python manage.py migrate

4/ Peupler les données des tables de référence, en l'occurence la table genre

>python manage.py loaddata data.json

5/ Créer un super administrateur de l'application

> python manage.py createsuperuser

>Renseigner les informations du super utilisateur à créer

Lancement de l'application web

>python manage.py runserver

L'enregistrement (création et mise à jour) et la suppression des données s'effectuent via l'interface administration avec authentification

  II/ Pipeline CI / CD
  
Pour cette étape, nous avons tout d'abord voulu utiliser Netfily pour la Pipeline CI / CD.
Arpès une configuration réussie, il a fallu payer un abonnement annuel afin d'obtenir un nom de domaine viable. Nous avons donc abandonner cet outil.

Nous sommes par la suite passé à Gitlab ou après la configuration, le built était réussi, cependant le test ne fonctionnais pas car il ne prennais pas en charge Python.
Gitlab ne prenant pas en charge du python, il nous renvoie vers HEROKU.

HEROKU etant adapté au language Python, après la réalisation du CI, fichier .gitlab-ci.yml, le deployement fonctionne !!
		https://cinema-devops-ing4.herokuapp.com/
	
     AUTEURS :
     Platonov Daniel, Trimeche Souhail et une participation de Smith Ambroise
     APPSI01 ING4

Liens : - https://cinema-devops-ing4.herokuapp.com/
	- https://gitlab.com/souhail_trimeche/cinema
