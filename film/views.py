from django.shortcuts import render
from .models import *
# Create your views here.
##Ici on crée un methode qui permet de récupérer la liste des films de notre cinéma
###Pour récupérer la liste des films, il faut requêter la bdd via orm django 

def get_all_films(request):
    result= Film.objects.all()
    return render(request, "liste.html", {"films":result})
