from django.db import models

# Create your models here.
class Genre(models.Model):
    libelle = models.CharField(max_length=20)

    def __str__(self):
        return self.libelle

class Seance(models.Model):
    date = models.DateField(null=True)
    heure = models.TimeField(null=True)

    def __str__(self):
        return "{} {}".format(self.date, self.heure)

class Film(models.Model):
    titre = models.CharField(max_length=200)
    resume = models.TextField()
    realisateur = models.CharField(max_length=100)
    genre = models.ForeignKey(Genre, on_delete=models.SET_NULL, null=True)
    seances = models.ManyToManyField(Seance)
    duree = models.TimeField(null=True)
    image = models.ImageField(upload_to='media', blank=True, null=True)

    def __str__(self):
        return self.titre
    def was_published_recently(self):
        return self.pub_date >= timezone.now() -datetime.timedelta(days=7)
    def image_tag(self):
        return mark_safe('<img src="/media/%s" width="150" height="150" />' % (self.image))

    image_tag.short_description = 'Image'

    
