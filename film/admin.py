from django.contrib import admin
from .models import *

admin.site.register(Film)
admin.site.register(Genre)
admin.site.register(Seance)
fields = ['image_tag']
readonly_fields = ['image_tag']
