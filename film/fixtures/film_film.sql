-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mar. 22 déc. 2020 à 14:01
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `cinema`
--

-- --------------------------------------------------------

--
-- Structure de la table `film_film`
--

DROP TABLE IF EXISTS `film_film`;
CREATE TABLE IF NOT EXISTS `film_film` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(200) NOT NULL,
  `resume` longtext NOT NULL,
  `realisateur` varchar(100) NOT NULL,
  `genre_id` int(11) DEFAULT NULL,
  `duree` time(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `film_film_genre_id_2d8a0b89` (`genre_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `film_film`
--

INSERT INTO `film_film` (`id`, `titre`, `resume`, `realisateur`, `genre_id`, `duree`) VALUES
(1, 'L\'OUBLI QUE NOUS SERONS', 'Colombie, années 1980. Le docteur Hector Abad Gomez lutte pour sortir les habitants de Medellin de la misère. Malgré les menaces qui pèsent sur lui, il refuse d’être réduit au silence. Le destin de ce médecin engagé et père de famille dévoué se dessine à travers le regard doux et admiratif de son fils.\r\nAdapté de faits réels, NOUS SOMMES DÉJÀ L’OUBLI QUE NOUS SERONS est à la fois le portrait d’un homme exceptionnel, une chronique familiale et l’histoire d’un pays souvent marqué par la violence.', 'Fernando Trueba', 3, '02:16:00.000000'),
(2, 'AVATAR 2', 'Retour sur Pandora avec la suite d\'\"Avatar\".', 'James Cameron', 7, '01:55:00.000000'),
(3, 'LES TUCHE 4', 'Après avoir démissionné de son poste de président de la république, Jeff et sa famille sont heureux de retrouver leur village de Bouzolles. A l’approche des fêtes de fin d’année, Cathy demande un unique cadeau : renouer les liens avec sa sœur Maguy, et son mari Jean-Yves avec qui Jeff est fâché depuis 10 ans.\r\nLa réconciliation aurait pu se dérouler sans problème, sauf que lors d’un déjeuner, Jeff et Jean-Yves, vont une nouvelle fois trouver un sujet de discorde : NOËL. Cette querelle familiale qui n’aurait jamais dû sortir de Bouzolles va se transformer en bras de fer entre Jeff et un géant de la distribution sur Internet.', 'Olivier Baroux', 2, '01:45:00.000000'),
(4, 'MARVEL SONY UNTITLED SPIDER-MAN: FAR FROM HOME SEQUEL', 'La troisième aventure de Spider-Man devrait explorer les réalités parallèles de l\'univers Marvel.', 'Jon Watts', 1, '02:00:00.000000'),
(5, 'LES CROODS 2 : UNE NOUVELLE ÈRE', 'Les Croods ont survécu à leur part de dangers et de catastrophes mais ils vont maintenant devoir relever leur plus grand défi : rencontrer une autre famille.\r\nLes Croods ont besoin d\'un nouvel endroit où habiter. La famille préhistorique part alors en quête d\'un endroit plus sûr. Quand ils découvrent un paradis idyllique entouré de murs, ils pensent que tous leurs problèmes sont résolus... Mais une famille y vit déjà : les Bettermans.\r\nAvec leur cabane dans les arbres, leurs inventions étonnantes et leurs hectares irrigués de produits frais, Les Bettermans sont bien au-dessus des Croods sur l\'échelle de l\'évolution. Ils accueillent les Croods avec joie, mais les tensions ne tardent pas à s\'intensifier entre la famille des grottes et la famille moderne.\r\nMais une nouvelle menace va propulser les deux familles dans une aventure épique hors des murs, ce qui les obligera à accepter leurs différences et à se servir des forces des uns et des autres.', 'Joel Crawford', 2, '01:36:00.000000'),
(6, 'LE DISCOURS', 'Adrien est coincé. Coincé à un dîner de famille où papa ressort la même anecdote que d’habitude, maman ressert le sempiternel gigot et Sophie, sa soeur, écoute son futur mari comme s’il était Einstein. Alors il attend. Il attend que Sonia réponde à son sms, et mette fin à la « pause » qu’elle lui fait subir depuis un mois. Mais elle ne répond pas. Et pour couronner le tout, voilà que Ludo, son futur beau-frère, lui demande de faire un discours au mariage… Oh putain, il ne l’avait pas vu venir, celle-là ! L’angoisse d’Adrien vire à la panique. Mais si ce discours était finalement la meilleure chose qui puisse lui arriver ?', 'Laurent Tirard', 2, '01:28:00.000000'),
(7, 'MOURIR PEUT ATTENDRE', 'James Bond a quitté les services secrets et coule des jours heureux en Jamaïque. Mais sa tranquillité est de courte durée car son vieil ami Felix Leiter de la CIA débarque pour solliciter son aide : il s\'agit de sauver un scientifique qui vient d\'être kidnappé. Mais la mission se révèle bien plus dangereuse que prévu et Bond se retrouve aux trousses d\'un mystérieux ennemi détenant de redoutables armes technologiques…', 'Cary Joji Fukunaga', 1, '02:43:00.000000'),
(8, 'DUNE', 'L\'histoire de Paul Atreides, jeune homme aussi doué que brillant, voué à connaître un destin hors du commun qui le dépasse totalement. Car s\'il veut préserver l\'avenir de sa famille et de son peuple, il devra se rendre sur la planète la plus dangereuse de l\'univers – la seule à même de fournir la ressource la plus précieuse au monde, capable de décupler la puissance de l\'humanité. Tandis que des forces maléfiques se disputent le contrôle de cette planète, seuls ceux qui parviennent à dominer leur peur pourront survivre…', 'Denis Villeneuve', 7, '01:35:00.000000'),
(9, 'PROFESSION DU PÈRE', 'Emile, 12 ans, vit dans une ville de province dans les années 1960, aux côtés de sa mère et de son père. Ce dernier est un héros pour le garçon. Il a été à tour à tour était chanteur, footballeur, professeur de judo, parachutiste, espion, pasteur d\'une Église pentecôtiste américaine et conseiller personnel du général de Gaulle. Et ce père va lui confier des missions dangereuses pour sauver l’Algérie, comme tuer le général.', 'Jean-Pierre Améris', 3, '01:45:00.000000'),
(10, 'MONSTER HUNTER', 'Notre monde en cache un autre, dominé par de puissants et dangereux monstres. Lorsque le Lieutenant Artemis et son unité d’élite traversent un portail qui les transporte dans ce monde parallèle, ils subissent le choc de leur vie. Au cours d’une tentative désespérée pour rentrer chez elle, le brave lieutenant rencontre un chasseur mystérieux, qui a survécu dans ce monde hostile grâce à ses aptitudes uniques. Faisant face à de terrifiantes et incessantes attaques de monstres, ces guerriers font équipe pour se défendre et trouver un moyen de retourner dans notre monde.', 'Paul W.S. Anderson', 1, '01:43:00.000000');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
